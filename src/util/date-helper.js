
const checkDateArgument = date => {
  if(date instanceof Date === false) {
    throw new Error("date argument must be an instance of a Date")
  }
}

export const getStartOfDay = (date) => {
  checkDateArgument(date);
  let startOfDay = new Date(date.getTime());
  startOfDay.setHours(0, 0, 0, 0);
  return startOfDay;
}

export const getEndOfDay = (date) => {
  checkDateArgument(date);
  let endOfDay = new Date(date.getTime());
  endOfDay.setHours(23, 59, 59, 999);
  return endOfDay;
}

export const getEndOfMonth = (date) => {
  checkDateArgument(date);
  const month = date.getMonth();
  const year = date.getFullYear();

  const nextMonth = month === 11 ? 0 : month + 1;
  const nextYear = month === 11 ? year + 1 : year; 

  return new Date(new Date(nextYear, nextMonth).getTime() - 1);
}

export const getTotalDaysInMonth = (date) => {
  return getEndOfMonth(date).getDate();
}

export const getTotalHours = (date, otherDate) => {
  const time = otherDate - date;
  return ((time / 1000 /* seconds */) / 60 /* minutes */) / 60 /* hours */;
}

export const getTotalDays = (date, otherDate) => {  
  return getTotalHours(date, otherDate) / 24;
}

export const getTotalWeeks = (date, otherDate) => {
  return getTotalDays(date, otherDate) / 7;
}

export const getWeek = (date) => {
  checkDateArgument(date);
  return date.getDate() % 7;
}

export const addMinutes = (date, minutes) => {
  checkDateArgument(date);
  return new Date(date.getTime() + ((minutes * 60) * 1000));
}

export const addHours = (date, hours) => {
  checkDateArgument(date);
  return new Date(date.getTime() + (((hours * 60) * 60) * 1000));
}

export const addDays = (date, days) => {
  checkDateArgument(date);
  return addHours(date, 24 * days);
}

export const addWeeks = (date, weeks) => {
  checkDateArgument(date);
  return addDays(date, 7 * weeks);
}