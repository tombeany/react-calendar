
/**
 * Compares is two appointments overlap
 * @param {any} appointment the appointment to compare
 * @param {any} otherAppointment the other appointment to compare
 */
export const doesAppointmentOverlap = (appointment, otherAppointment) => {
  /** 
   * does the appointment span the other appointment completely?
   * ----- appointment from
   * |    |
   * |    |
   * ------ other appointment from
   * |    |
   * ------ other appointment to
   * |    |
   * ----- appointment to
   */
  if(appointment.from <= otherAppointment.from && appointment.to >= otherAppointment.to)
    return true;
  /** 
   * does the appointment end sit within the other appointment
   * ------ appointment from
   * |    |
   * |    |
   * ------ other appointment from
   * |    |
   * ------ appointment to
   * |    |
   * ------ other appointment to
   */
  if(appointment.to <= otherAppointment.to && appointment.to > otherAppointment.from)
    return true;
  /** 
   * does the appointment start sit within the other appointment
   * ------ othe appointment from
   * |    |
   * |    |
   * ------ appointment from
   * |    |
   * ------ other appointment to
   * |    |
   * ------ appointment to
   */
  if(appointment.from >= otherAppointment.from && appointment.from < otherAppointment.to)
    return true;

  return false;
};