export const groupBy = (array, keyGetter) => {
  return array.reduce((agg, next) => {
    const key = keyGetter(next);
    (agg[key] = agg[key] || []).push(next);
    return agg;
  }, {});
}

export const debounce = (fn, timeout) => {
  let handle = 0;
  return () => {
    if(handle) clearTimeout(handle);
    handle = setTimeout(fn, timeout);
  };  
}

export const throttle = (fn, timeout) => {  
  let lastCall = 0;
  return () => {  
    const now = new Date().getTime();
    lastCall = (lastCall < now ? now : lastCall) + timeout;
    setTimeout(fn, lastCall - now);
  }
}