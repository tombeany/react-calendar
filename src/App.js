import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import {
  addHours,
  getStartOfDay
} from './util/date-helper';

import Calendar from './components/Calendar';
import AppBar from './components/navigation/AppBar';
import AppPanel from './components/navigation/AppPanel';
import CalendarSidePanel from './components/CalendarSidePanel';

const now = new Date();
const today = getStartOfDay(now);
const workStart = addHours(today, 9);
const workEnd = addHours(today, 17.5);

const from = workStart;
const to = workEnd;
const step = 15;

class App extends Component {
  state = {
    panelOpen: false,
    activeIndex: -1,
    mode: 'stacked',
  }

  componentDidMount() {
    // const calendars = groupBy([], item => item.location);
    
    // setInterval(() => {      
    //   const events = []; // this.state.roomA.map((item) => Object.assign({}, item));
    //   const rnd = Math.random();
    //   const timeRand = Math.random();
    //   const eventIndex = Math.floor(events.length * rnd);
    //   const hoursToAdd = 2;

    //   const nextEvent = events[eventIndex];      
    //   nextEvent.from = new Date(from.getTime() + ((to - from) * timeRand));;
    //   nextEvent.to = addHours(nextEvent.from, hoursToAdd * Math.random());;

    //   const doesClash = events
    //     .some(x => x.id !== nextEvent.id && doesEventOverlap(nextEvent, x));

    //   if(!doesClash) {
    //     events[eventIndex] = nextEvent;
    //     // this.setState({
    //     //   roomA: events
    //     // });
    //   }
    // }, 1000)
  }

  handleLayoutClick = evt => {
    const mode = this.state.mode === 'stacked' ? 'column' : 'stacked';
    this.setState({ mode, activeIndex: -1 });
  }

  handleSidePanelClick = evt => {
    if(this.state.panelOpen === true) {
      this.setState({ panelOpen: false});
    }
    if(this.state.panelOpen === false) {
      this.setState({ panelOpen: true});
    }
  }

  render() {
    return (
      <div className="App">
        <AppBar />
        <div className="app-container">          
          <CalendarSidePanel 
            open={this.state.panelOpen}
            onExpanderClick={this.handleSidePanelClick} >
          </CalendarSidePanel>
          <AppPanel>
            <Calendar 
              from={from}
              to={to}
              step={step}
            />
          </AppPanel>
        </div>
      </div>
    );
  }
}

export default App;
