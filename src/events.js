import {
  getStartOfDay,
  addHours
} from './util/date-helper'


const today = getStartOfDay(new Date())

const eventsA = [
  {
    id: 1,
    location: "Room 1",
    title: 'Jeff Morris',
    type: 'eye-exam',
    from: addHours(today, 11),
    to: addHours(today, 11.25)
  },
  {
    id: 2,
    location: "Room 1",
    title: 'Jason Dyke',
    type: 'lens-fitting',
    from: addHours(today, 10),
    to: addHours(today, 11)
  },
  {
    id: 3,
    location: "Room 1",
    title: 'Tom Bean',
    type: 'check-up',
    from: addHours(today, 15.25),
    to: addHours(today, 15.5)
  },
  {
    id: 4,
    location: "Room 1",
    title: 'Jo',
    type: 'emergency',
    from: addHours(today, 14),
    to: addHours(today, 15)
  },
  {
    id: 5,
    location: "Room 1",
    title: 'Dan Brannon',
    type: 'eye-exam',
    from: addHours(today, 9),
    to: addHours(today, 10)
  },
  {
    id: 6,
    location: "Room 1",
    title: 'Malcolm Lippett',
    type: 'lens-fitting',
    from: addHours(today, 10),
    to: addHours(today, 11)
  },
  {
    id: 7,
    location: "Room 1",
    title: 'Ceri Williams',
    type: 'check-up',
    from: addHours(today, 11.25),
    to: addHours(today, 11.75)
  },
  {
    id: 8,
    location: "Room 1",
    title: 'Adam Eamonson',
    type: 'emergency',
    from: addHours(today, 14),
    to: addHours(today, 15)
  }
];

const eventsB = [
  {
    id: 9,
    location: "Room 2",
    title: 'Sam',
    from: addHours(today, 9.5),
    to: addHours(today, 11)
  },
  {
    id: 10,
    location: "Room 2",
    title: 'Jason Dyke',
    type: 'emergency',
    from: addHours(today, 13),
    to: addHours(today, 13.5)
  },
  {
    id: 11,
    location: "Room 2",
    title: 'Maxim',
    type: 'lens-fitting',
    from: addHours(today, 17),
    to: addHours(today, 17.5)
  }
];

const eventsC = [
  {
    id: 12,
    location: "Room 3",
    title: 'Nerine Proudlock',
    type: 'check-up',
    from: addHours(today, 9.5),
    to: addHours(today, 9.75)
  },
  {
    id: 13,
    location: "Room 3",
    title: "Chris 'Stormin' Norman",
    from: addHours(today, 10),
    to: addHours(today, 11)
  },
  {
    id: 14,
    location: "Room 3",
    type: 'lens-fitting',
    title: 'Pete Jespers',
    from: addHours(today, 13),
    to: addHours(today, 14)
  },
  {
    id: 15,
    location: "Room 3",
    type: 'emergency',
    title: 'Andrew Harrison',
    from: addHours(today, 12),
    to: addHours(today, 12.15)
  }
];

const eventsD = [
  {
    id: 16,
    location: "Room 4",
    title: 'Ande Walsh',
    from: addHours(today, 12),
    to: addHours(today, 12.5)
  },
  {
    id: 17,
    location: "Room 4",
    title: 'Jason Dyke',
    from: addHours(today, 13),
    to: addHours(today, 13.5)
  },
  {
    id: 18,
    location: "Room 4",
    title: 'Terry Jenkins',
    from: addHours(today, 14.25),
    to: addHours(today, 14.5)
  },
  {
    id: 19,
    location: "Room 4",
    title: 'Lee Atkinson',
    from: addHours(today, 14.75),
    to: addHours(today, 15)
  }
];

export {
  eventsA,
  eventsB,
  eventsC,
  eventsD
}