import React from 'react'
import ActivityCard from './ActivityCard'

function createActivity(activity) {
  switch(activity.type) {
    case 'eye-exam':
      return (
        <ActivityCard 
          key={activity.id} 
          bgColour='#538e55' 
          {...activity} 
        />
      )
    case 'lens-fitting':
      return (
        <ActivityCard 
          key={activity.id} 
          bgColour='#ef7523' 
          {...activity} 
        />
      )
    case 'check-up':
      return (
        <ActivityCard 
          key={activity.id} 
          bgColour='#38b4e5' 
          {...activity} 
        />
      )
    case 'emergency':
      return (
        <ActivityCard 
          key={activity.id} 
          bgColour='#c04756'
          {...activity} 
        />
      )
    default:
      return (
        <ActivityCard 
          key={activity.id} 
          bgColour='#263770' 
          {...activity} 
        />
      )
  }
}

export default createActivity

