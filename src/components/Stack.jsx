import React, { Component } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import StackSlot from './StackSlot'

const StyledStack = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`

export default class Stack extends Component {
  static propTypes = {
    /**
     * The index of the current active slot, -1 if none
     */
    activeIndex: PropTypes.number,
    /**
     * Called when a slot is clicked, index of slot is passed as argument
     */
    onSlotClick: PropTypes.func
  }

  constructor(props) {
    super(props);

    this.state = {
      containerWidth: 0
    }

    this.containerRef = React.createRef();
    this.handleWindowResize = this.handleWindowResize.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleWindowResize);

    this.handleWindowResize();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowResize);
  }

  handleWindowResize() {
    const containerWidth = this.containerRef.current && this.containerRef.current.clientWidth;
    if(containerWidth !== this.state.containerWidth) {
      this.setState({
        containerWidth: containerWidth
      });
    }
  }

  handleSlotClick = (evt, index) => {    
    this.props.onSlotClick && this.props.onSlotClick(evt, index);
  }
  

  render() {
    const {
      activeIndex,
      children,
      onSlotClick,
      ...rest
    } = this.props;

    const fullWidth = 75;
    const total = children.length;
    const containerWidth = this.state.containerWidth;
    const hasSelected = activeIndex >= 0;
    const slotWidth = containerWidth / total;

    return (
      <StyledStack ref={this.containerRef} className="stack" {...rest}>
        { React.Children.map(children, (child, index) => {

          const isActive = index === activeIndex;
          const isLeft = hasSelected && index < activeIndex;
          const isRight = hasSelected && index > activeIndex;
          
          let width = hasSelected ? ((containerWidth / 100) * fullWidth) : slotWidth;
          let offsetLeft = slotWidth * index;
          let layer = isActive ? total + 1 : index;
          
          const spacing = (containerWidth - width) / (total - 1);

          if(isLeft) {
            offsetLeft = (spacing * index);
            layer = total - (activeIndex - index);
          }

          if(isRight) {
            offsetLeft = (spacing * index);
            layer = total - index;
          }

          if(isActive) {
            offsetLeft = (spacing * index);
            //width = containerWidth - (spacing * (total - 1));
          }

          return (
            <StackSlot
              key={index}
              layer={layer}
              index={index}
              activeIndex={activeIndex}
              isActive={isActive}
              totalSlots={total}
              offset={offsetLeft}
              width={width}
              spacing={spacing}
              onClick={(evt) => this.handleSlotClick(evt, index)}
              >
              {child}
            </StackSlot>
          )
        }) }
      
      </StyledStack>
    )
  }
}
