import React from 'react'
import styled from 'styled-components'

const StyledButton = styled.div`
  width: 35px
  height: 25px;
  position: relative;
`

const StyledCol = styled.div.attrs(props => ({
  style: {
    width: props.active ? '70%' : `${ 100 / props.total }%`,
    left: props.active ? `${ (30 / (props.total - 1)) * props.index }%` : `${ (100 / props.total) * props.index }%`,
    zIndex: props.total - props.index
  }
}))`
  position: absolute;
  top: 0;
  height: 100%; 
  transition: all .25s ease;
`
  
const StyledStack = styled.div`
  border: .5px solid black;
  height: 100%;
  margin: 0 1px;
  box-sizing: border-box;
  background-color: #f2f2f2;
`

const renderCols = (active, amount) => {
  let cols = [];
  for(let i=0; i < amount; i++) {
    cols.push(
      <StyledCol 
        key={i} 
        index={i}
        total={amount}
        active={active}
      >
        <StyledStack />
      </StyledCol>
    )
  }
  return cols;
}

const StackButton = props => {
  const {
    active,
    cols,
    ...rest
  } = props;

  return (
    <StyledButton {...rest} >
      { renderCols(!active, cols) }
    </StyledButton>
  )
}

export default StackButton;