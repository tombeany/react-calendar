import React from 'react'
import styled from 'styled-components'

const StyledTimeSegment = styled.div.attrs(props => ({
  style: { borderTop: props.validDrop ? '1px solid red' : 'none'}
}))`
  width: 100%;
  flex: 1 1 auto;
  position: relative;
  text-align: ${ props => props.stackedLeft ? 'left' : 'right' };
  background-color: ${ props => props.alternate ? '#e4e4e4' : '#f2f2f2' };
  overflow: hidden;
`
const StyledTimeLabel = styled.label`
  color: #cccccc
`

const TimeSegment = props => {
  const {
    slotNo,
    from,
    to,
    stackedLeft,
    ...rest
  } = props;

  return (
    <StyledTimeSegment        
      alternate={ ((slotNo % 2) === 0) }
      stackedLeft={stackedLeft}
      {...rest} >
      <StyledTimeLabel>
        <span>{from.toLocaleTimeString()}</span>
      </StyledTimeLabel>
    </StyledTimeSegment>
  )
}

export default TimeSegment