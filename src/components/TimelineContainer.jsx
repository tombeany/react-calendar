import React, { Component } from 'react'
import styled from 'styled-components'

const StyledFrame = styled.div.attrs(props => ({
  style : {
    padding: props.stacked ? '0' : '10px 5px',
    boxShadow: !props.stacked ? 'none' : props.active ? `1px 1px 20px 8px rgba(0, 0, 0, .2)` : `1px 1px 8px 2px rgba(0, 0, 0, .2)`
  }
}))`
  height: 100%;
  background-color: white;
  box-sizing: border-box;
`
const StyledInner = styled.div`
  height: 100%;
`

const TimelineTitle = styled.div.attrs(props => ({
  style: {
    left: props.stackedLeft ? 0 : props.stackedRight ? '100%' : '50%',
    transform: `translate3d(${ props.stackedLeft ? 0 : props.stackedRight ? '-100%' : '-50%' }, 0, 0)`
  }
}))`
  position: absolute;
  top: 0;  
  z-index: 100;
  background-color: white;
  padding: 6px 12px;
  font-weight: bold;
  line-height: 1em;
  white-space: nowrap;
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;
  transition: left .25s ease-out, right .25s ease-out;
`

export default class ScrollableTimeline extends Component {

  render() {
    const {
      title,
      children,
      ...rest
    } = this.props;

    return (
      <StyledFrame
        className="timeline-frame"
        stacked={this.props.stacked === true}
        active={this.props.active === true}
      >
        <TimelineTitle
          stacked={this.props.stacked === true}
          stackedLeft={this.props.stackedLeft === true}
          stackedRight={this.props.stackedRight === true}
        >
          {title}
        </TimelineTitle>
        <StyledInner className="timeline-frame-inner">
          { React.Children.map(children, (child) => React.cloneElement(child, {...rest})) }
        </StyledInner>
      </StyledFrame>
    )
  }
}
