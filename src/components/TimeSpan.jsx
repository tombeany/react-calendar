import React from 'react'

export default function TimeSpan(props) {
  const {
    from,
    to
  } = props;
  return (
    <label>
      <span>{from.toLocaleTimeString()}</span>
      <span> - </span>
      <span>{to.toLocaleTimeString()}</span>
    </label>
  )
}
