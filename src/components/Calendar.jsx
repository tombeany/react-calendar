import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Stack from './Stack';
import TimelineContainer from './TimelineContainer'
import Timeline from './Timeline'
import StackButton from './StackButton'
import withController from './CalendarController'
import { debounce } from '../util'

const StyledContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
`
const StyledTopBar = styled.div`
  display: flex;
  flex: 0 1 auto;
  background-color: #f2f2f2;
  justify-content: space-between;
  align-items: center;
  padding: 5px 10px;
`

const StyledDate = styled.label`
  font-size: 2em;
  text-transform: uppercase;
`

const StyledCalendarArea = styled.div.attrs(props => ({
  style: { overflowY : props.stacked ? 'auto' : 'hidden' }
}))`
  flex: 1 1 auto;
  overflow-y: hidden;
`

class Calendar extends Component {
  static propTypes = {
    from: PropTypes.instanceOf(Date),
    to: PropTypes.instanceOf(Date),
    step: PropTypes.number
  }

  constructor(props) {
    super(props);

    this.handleResize = this.handleResize.bind(this);
    this.windowResize = debounce(this.handleResize, 250);
  }

  state = {
    activeIndex: -1,
    calendarDimensions: {}    
  }

  calendarAreaRef = React.createRef();

  componentDidMount() {
    window.addEventListener('resize', this.windowResize);
    this.handleResize();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.windowResize);
  }

  handleResize = () => {
    console.log('resizing');
    const elem = this.calendarAreaRef && this.calendarAreaRef.current;
    const calendarHeight = (elem && elem.clientHeight) || 0;
    const calendarWidth = (elem && elem.clientWidth) || 0;

    if(calendarHeight !== this.state.calendarDimensions.height || calendarWidth !== this.state.calendarDimensions.width) {
      this.setState({
        calendarDimensions: {
          width: calendarWidth,
          height: calendarHeight
        }
      })
    }
  }

  handleSlotClick = (evt, index) => {
    if(index !== this.state.activeIndex)
      this.setState({ activeIndex: index });
  }

  handleLayoutClick = evt => {    
    this.setState({ activeIndex: this.state.activeIndex === -1 ? 0 : -1 });
  }

  handleApptDrop = (evt, appt, time, location) => {

    this.props.onApptDropped && this.props.onApptDropped(evt, appt, time, location);
  }

  render() {
    const {
      from,
      to,
      step,
      locations,
      onApptDropped,
      ...rest
    } = this.props;

    const scaleFactor = 3;
    const scale = this.state.activeIndex > -1 ? scaleFactor : 1;
    const hasSelected = this.state.activeIndex !== -1;

    return (
      <StyledContainer className="calendar" {...rest}>
        <StyledTopBar className="calendar-bar">
          <div>
            <StyledDate>{ from.toLocaleString('en-GB', { weekday: 'long', day: 'numeric', month: 'short', year: 'numeric' }) }</StyledDate>            
          </div>
          <div>
            <StackButton 
              type="button"
              cols={4}
              active={hasSelected} 
              onClick={this.handleLayoutClick} 
            />
            
          </div>
        </StyledTopBar>
        <StyledCalendarArea className="calendar-area" ref={this.calendarAreaRef} stacked={this.state.activeIndex > -1}>
          <Stack 
            activeIndex={this.state.activeIndex}
            style={{ height: this.state.calendarDimensions && this.state.calendarDimensions.height ? `${this.state.calendarDimensions.height}px` : 'auto' }}
            onSlotClick={(evt, index) => this.handleSlotClick(evt, index)}
          >
            {
              locations.map((room, i) => (
                <TimelineContainer key={room.id} title={room.title}>              
                  <Timeline
                    from={from}
                    to={to}
                    step={step}
                    activities={room.events}
                    scale={scale}
                    onApptDropped={(evt, appt, time) => this.handleApptDrop(evt, appt, time, room)}
                  />
                </TimelineContainer>
              ))
            }
            
            {/* <TimelineContainer  title="Room 02">
              <Timeline
                from={from}
                to={to}
                step={step}
                events={eventsB}
                scale={this.state.activeIndex > -1 ? scaleFactor : 1}
              />
            </TimelineContainer>
            <TimelineContainer  title="Room 03">
              <Timeline
                from={from}
                to={to}
                step={step}
                events={eventsC}
                scale={this.state.activeIndex > -1 ? scaleFactor : 1}
              />
            </TimelineContainer>
            <TimelineContainer  title="Room 04">
              <Timeline
                from={from}
                to={to}
                step={step}
                events={eventsD}
                scale={this.state.activeIndex > -1 ? scaleFactor : 1}
              />
            </TimelineContainer> */}
            {/* <TimelineContainer  title="Room 05">
              <Timeline
                from={from}
                to={to}
                step={step}
                events={eventsD}
                scale={this.state.activeIndex > -1 ? scale : 1}
              />
            </TimelineContainer>
            <TimelineContainer  title="Room 06">
              <Timeline
                from={from}
                to={to}
                step={step}
                events={eventsD}
                scale={this.state.activeIndex > -1 ? scale : 1}
              />
            </TimelineContainer> */}
          </Stack>
        </StyledCalendarArea>
      </StyledContainer>
    )
  }
}

export default withController(Calendar)
