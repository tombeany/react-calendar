import React, { Component } from 'react'
import styled from 'styled-components'

const StyledStackSlot = styled.div.attrs(props => ({ 
  style: {
    left: `${props.offset + props.nudge}px`,
    width: `${props.width}px`,
    zIndex: props.active ? props.total + 1 : props.layer    
  }
}))`
  position: absolute;
  top: 0;
  height: 100%;
  transition: left .35s cubic-bezier(.7, -.5, .3, 1.5), width .35s ease;
`

const StyledStackSlotInner = styled.div`
  height: 100%;
`

export default class StackSlot extends Component {
  
  state = {
    hovered: false
  }

  handlePointerOver = evt => {
    if(!this.state.hovered)
      this.setState({ hovered: true });
  }

  handlePointerOut = evt => {
    if(this.state.hovered)
      this.setState({ hovered: false });
  }

  render() {
    const {
      layer,
      index,
      activeIndex,
      isActive,
      totalSlots,
      offset,
      width,
      spacing,
      children,
      onClick,
      ...rest
    } = this.props;

    const isLeft = activeIndex > -1 && index < activeIndex;
    const isRight = activeIndex > -1 && index > activeIndex;
    const movement = 50;
    const hovevered = this.state.hovered;
    const nudge = (hovevered && isLeft) ? -movement : (hovevered && isRight) ? movement : 0;
    return (
      <StyledStackSlot
          className="slot"
          layer={layer}
          index={index}
          active={isActive}
          total={totalSlots}
          offset={offset}
          width={width}
          spacing={spacing}
          nudge={nudge}
          onClick={(evt) => onClick && onClick(evt, index)}
          onPointerEnter={this.handlePointerOver}
          onPointerLeave={this.handlePointerOut}
          {...rest}
          >
          <StyledStackSlotInner className="slot-inner" >
            { 
              React.Children.map(children, (child, index) => {
                return React.cloneElement(child, {
                  active: !!isActive ,
                  stacked: activeIndex > -1,
                  stackedLeft: !!isLeft,
                  stackedRight: !!isRight
                });
              })
            }
          </StyledStackSlotInner>
        </StyledStackSlot>
    )
  }
}
