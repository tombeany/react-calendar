import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import TimeSpan from './TimeSpan'

//box-shadow: 2px 2px 5px 0px rgba(0, 0, 0, .5);
const StyledCard = styled.div`
  box-sizing: border-box;
  border-radius: 4px;
  height: 100%;
  display: flex;
  overflow: hidden;
  font-size: .75em;
  color: #263770;
  box-shadow: 1px 2px 4px 0 rgba(0, 0, 0, .25);
`

const StyledInnerCard = styled.div`
  width: 90%;
  height: 100%;
  display: flex;
  flex-direction: row;
  align-items: stretch;
  background-color: white;
  border-right: 1px solid ${ props => props.bgColour }
`

const StyledTimeView = styled.div`
  box-sizing: border-box;
  width: 10%;  
  height: 100%;  
  border-top: 1px solid white;
  border-right: 1px solid white;
  border-bottom: 1px solid white;
  border-left: none;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  background-color: rgba(255, 255, 255, .2);
`

const StyledEventKey = styled.div`
  flex: 0 1 auto;
  margin-right: 1em;
`

const StyledEventDetails = styled.div`
  flex: 1 1 auto;
  display: flex;
  flex-direction: column;
  flex: 1 0 10px;
`

const StyledFor = styled.label`
  font-size: 1em;
  font-weight: bold;
`

const StyledEventType = styled.div`
  flex: 1 1 10%;
  padding: 5px;
  color: white;
  background-color: ${ props => props.bgColour }
`

export default class ActivityCard extends Component {
  static propTypes = {
    from: PropTypes.instanceOf(Date),
    to: PropTypes.instanceOf(Date)
  }

  render() {
    const {
      title,
      from,
      to,
      type,
      bgColour
    } = this.props;

    return (
      <StyledCard >
        <StyledInnerCard bgColour={bgColour || 'white' }>
          <StyledEventKey>

          </StyledEventKey>
          <StyledEventDetails className="event-details">
            <StyledFor>{title}</StyledFor>            
            <TimeSpan from={from} to={to} />
          </StyledEventDetails>
          <StyledEventType bgColour={bgColour || 'white' }>
            {type}
          </StyledEventType>
        </StyledInnerCard>
        <StyledTimeView />
      </StyledCard>
    )
  }
}
