import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import StackSlot from './StackSlot'

const StyledStack = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`

export default class SortableStack extends Component {
  static propTypes = {
    
  }

  constructor(props) {
    super(props);

    this.state = {
      containerWidth: 0,
      registry: this.resetOrder()
    }

    this.containerRef = React.createRef();
    this.handleWindowResize = this.handleWindowResize.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleWindowResize);
    this.setState({
      registry: this.resetOrder()      
    })
    this.handleWindowResize();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowResize);
  }

  resetOrder() {
    const reg = {};
    React.Children.toArray(this.props.children).forEach((item, index) => reg[index] = index);
    return reg;
  }

  handleWindowResize() {

    const containerWidth = this.containerRef.current && this.containerRef.current.clientWidth;

    this.setState({
      containerWidth: containerWidth
    });
  }

  handleSlotClick = (evt, index) => {
    console.log(index);

    this.moveSlot(index, 0);
  }

  getPos = (index, registry) => {
    for(let key in registry) {
      if(registry[key] === index)
        return key;
    }
  }

  moveSlot = (fromIndex, toPos) => {
    const reg = {...this.state.registry};

    let fromPos = this.getPos(fromIndex, reg);

    const swap = reg[toPos];
    reg[toPos] = fromIndex;
    reg[fromPos] = swap;

    this.setState({ registry: reg });
  }

  getLayout = () => {
    const {
      mode,
      children      
    } = this.props;

    const totalWidth = this.state.containerWidth;
    const totalSlots = children.length;
    const offsetPx = 80;

    if(mode == 'stacked') {
      return {
        offsetPx: 80,
        slotWidth:  totalWidth - (offsetPx * (totalSlots - 1))
      }
    }

    const slotWidth = totalWidth / totalSlots;

    return {
      offsetPx: slotWidth,
      slotWidth: slotWidth
    }
  }

  render() {
    const {
      children
    } = this.props;

    const layout = this.getLayout();

    return (
      <StyledStack ref={this.containerRef}>
        { React.Children.map(children, (child, index) => {

          const pos = this.getPos(index, this.state.registry);
          //const slotWidth = totalWidth - (offsetPx * ((totalSlots - 1) - pos));

          return (
            <StackSlot
              key={index}
              index={index}
              pos={pos}
              total={children.length}
              offset={layout.offsetPx}
              width={layout.slotWidth}
              onClick={(evt) => this.moveSlot(index, 0)}
              >
              { child }
            </StackSlot>
          )
        }) }
      
      </StyledStack>
    )
  }
}
