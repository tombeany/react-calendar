import React, { Component } from 'react'
import styled from 'styled-components'
import createActivity from './ActivityTypes'
import TimeIndicator from './TimeIndicator'
import TimeSegment from './TimeSegment'

const StyledTimeLine = styled.div.attrs(props => ({
    style: { height: `${ (100 * props.scale) }%`  }
}))`
  display: flex;
  width: 100%;
  flex-direction: column;
  align-items: stretch;
  justify-content: flex-start;
  position: relative;
  transition: height .25s ease-out;
`

const StyledActivityWrapper = styled.div.attrs(props => ({
  style: { 
    top: `${ props.top }%`, 
    height: `${ props.height}%` 
  }
}))`
  position: absolute;
  width: 100%;
  left: 0;
  transition: all .5s ease-in-out;
`

export default class Timeline extends Component {

  state = {
    validDrop: false
  };

  getAppointmentData = evt => {
    const dataTransfer = evt.dataTransfer;
    
    let appt = dataTransfer.getData('appointment');
    if(appt)
      return appt;

    const strJson = dataTransfer.getData('application/json') || dataTransfer.getData('text/plain');
    if(strJson) {
      return JSON.parse(strJson);
    }
    throw new Error('Appointment could not be extracted from Drag event');
  }

  handleSegmentDragEnter = evt => {
    evt.preventDefault();
    evt.dataTransfer.dropEffect = "move";    

    if(this.state.validDrop === false) {
      this.setState({ 
        validDrop: true 
      });
    }
  }

  handleSegmentDragOver = evt => {
    evt.preventDefault();
    evt.dataTransfer.dropEffect = "move";

    this.props.onTimeSegmentDragOver && this.props.onTimeSegmentDragOver();
  }

  handleSegmentDragEnd = evt => {
    if(this.state.validDrop === true) {
      this.setState({
        validDrop: false
      });
    }
  }

  handleActivityDrop = (evt, time) => {
    const appt = this.getAppointmentData(evt);
    if(!appt) {
      throw new Error("Appointment data could not be parsed from drop");
    }

    if(this.state.validDrop === true) {
      this.setState({
        validDrop: false
      });
    }

    this.props.onApptDropped && this.props.onApptDropped(evt, appt, time);
  }

  handleActivityDragStart = (evt, appt) => {
    evt.dataTransfer.effectAllowed = 'move';

    evt.dataTransfer.setData('application/json', JSON.stringify(appt));
    evt.dataTransfer.setData('text/plain', JSON.stringify(appt));
    
  }
  
  createSlot = (index, from, to, stackedLeft) => (
    <TimeSegment 
      key={from.getTime()}
      slotNo={index++}
      from={from}
      to={to}
      stackedLeft={stackedLeft}
      onDragOver={evt => this.handleSegmentDragOver(evt)}
      onDragEnter={evt => this.handleSegmentDragEnter(evt)}
      onDragExit={evt => this.handleSegmentDragEnd(evt)}
      onDragLeave={evt => this.handleSegmentDragEnd(evt)}
      onDrop={evt => this.handleActivityDrop(evt, from)}
      />
  )

  renderSlots = (events, from, to, step, stackedLeft) => {
    
    // calculate step size in milliseconds
    const stepMs = ((step * 60) * 1000);

    let slots = [];

    let time = from;
    let index = 0;
    for(; time < to; time = new Date(time.getTime() + stepMs)) {      
      slots.push(this.createSlot(
          index++,
          time, 
          new Date(time.getTime() + stepMs), 
          stackedLeft
        )
      )
    }
    return slots;
  }

  renderActivities = (activities, from, to) => {
    const total = to - from;

    return activities.map((item, index) => {      
      const top = ((item.from - from) / total) * 100;
      const height = ((item.to - item.from) / total) * 100;

      return (
        <StyledActivityWrapper 
          draggable
          key={item.id} 
          top={top}
          height={height}
          onDragStart={ evt => this.handleActivityDragStart(evt, item) }
           >
          { createActivity(item) }
        </StyledActivityWrapper>
      )
    })
  }

  render() {
    const {
      from,
      to,
      step,
      scale,
      stackedLeft,
      stackedRight,
      activities,
      onApptDropped,
      ...rest
    } = this.props;

    return (
      <StyledTimeLine 
        className="timeline"
        scale={scale || 1}
        {...rest}
      >
        { this.renderSlots(activities, from, to, step, !stackedRight) }
        { this.renderActivities(activities, from, to) }
        <TimeIndicator from={from} to={to} />
      </StyledTimeLine>
    )
  }
}
