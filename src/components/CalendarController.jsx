import React from 'react';
import { groupBy } from '../util'
import {
  eventsA,
  eventsB,
  eventsC,
  eventsD
} from '../events';

const withController = (CalendarComponent) => {

  return class extends React.Component {

    state = {
      locations: []
    };

    componentWillMount = () => {
      const eventData = [...eventsA, ...eventsB, ...eventsC, ...eventsD];

      const groupedEvents = groupBy(eventData, e => e.location);

      let index = 1;
      let locations = [];
      for(let key in groupedEvents) {
        locations.push({
          id: index++, 
          title: key,
          events: groupedEvents[key]
        });
      }

      this.setState({ locations: locations });
    }

    handleMoveToLocation = (appt, time, location) => {
      // Get the location from state
      const locations = [...this.state.locations];

      const fromIndex = locations.findIndex(x => x.title === appt.location);
      const toIndex = locations.findIndex(x => x.id === location.id);

      const fromLocation = locations[fromIndex];
      const toLocation = locations[toIndex];


      const toEvents = [...toLocation.events];
      const fromEvents = [...fromLocation.events];
      
      // remove from old location
      const existing = fromEvents.findIndex(x => x.id === appt.id);
      if(existing) {
        fromEvents.splice(existing, 1);
        const oldLocation = Object.assign({}, fromLocation, { events: fromEvents });
        locations.splice(fromIndex, 1, oldLocation);
      }

      const diff = new Date(appt.to) - new Date(appt.from);

      appt.location = toLocation.title;
      appt.from = time;
      appt.to = new Date(time.getTime() + diff);

      toEvents.push(appt);
      const newLocation = Object.assign({}, toLocation, { events: toEvents });

      locations.splice(toIndex, 1, newLocation);
      
      this.setState({ locations });
    }

    handleMoveWithinLocation = (appt, time, location) => {
      if(appt.from === time) {
        console.log("Move to same time and location - do nothing");
        return;
      }

      // const location = this.state.locations.find(x => x.id === location.id);

      // const existing = location.events.findIndex(x => x.id === appt.id);
      // const evt = location[existing];

      // const diff = new Date(evt.to) - new Date(evt.from);

      // const newAppt = Object.assign({}, evt);
      // newAppt.from = time;
      // newAppt.to = new Date(time.getTime() + diff);
      
      // location.events.splice(existing, 1, newAppt);

      
    }

    handleApptDropped = (evt, appt, time, location) => {
      if(appt.location === location.title) {
        return this.handleMoveWithinLocation(appt, time, location);
      }

      return this.handleMoveToLocation(appt, time, location);
    }

    render() {
      const {
        locations,
        onApptDropped,
        ...rest
      } = this.props;
      return (
        <CalendarComponent
          locations={this.state.locations}
          onApptDropped={this.handleApptDropped}
          {...rest}
        />
      )
    }
  };
};

export default withController;