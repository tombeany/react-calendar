import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import SidePanel from './navigation/SidePanel'
import MonthCalendar from './MonthCalendar';

const StyledPanelExpander = styled.div`
  position: absolute;
  top: 50%;
  right: 0;
  transform: translate3d(0, -50%, 0);
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 40px;
  padding: 5px;
  cursor: pointer;
`

const StyledExpanderDot = styled.div`
  width: 3px;
  height: 3px;
  border-radius: 50%;
  background-color: white;
`

const Expander = props => (
  <StyledPanelExpander 
    onClick={props.onClick}
  >
    <StyledExpanderDot />
    <StyledExpanderDot />
    <StyledExpanderDot />
  </StyledPanelExpander>
)

const StyledMonthContainer = styled.div`
  font-size: 2em;
  letter-spacing: 6px;
  text-align: center;
  text-transform: uppercase;
  transform: rotate3d(0, 0, 1, ${ props => props.open ? '0deg' : '-90deg'});
  transform-origin: top left;
  transition: transform .25s linear;  
`

const StyledCalendarContainer = styled.div.attrs(props => ({
  style: { 
    opacity: props.open ? 1 : 0,
    transition: props.open ? 'opacity .25s ease-out .25s, max-height .25s ease-out' : 'max-height .25s ease-out',
    maxHeight: props.open ? 500 : 0
  }
}))`
  border-top: 1px solid white;
  overflow: hidden;
  transform: translate3d(0, 0, 0);
`
  
const StyledCalendarPadding = styled.div`
  padding: 15px 10px;
`

const StyledMiniCalendar = styled.div`
  position: absolute;
  right: 0;
  bottom: 0;
  width: 100%;
`

const MiniCalender = props => {
  const {
    open,
    year,
    month,
    day,
    ...rest
  } = props;

  const date = new Date(year, month, day);

  return (
    <StyledMiniCalendar className="mini-calendar" >
      <StyledMonthContainer className="mini-calendar__month" open={open}>
        <span>{date.toLocaleDateString('en-gb', { month: 'long'})}</span>
      </StyledMonthContainer>
      <StyledCalendarContainer className="mini-calendar__container" open={open}>
        <StyledCalendarPadding>
          <MonthCalendar 
            year={year} 
            month={month} 
            day={day}
            {...rest} 
          />
        </StyledCalendarPadding>
      </StyledCalendarContainer>
    </StyledMiniCalendar>
  )
}

export default class CalendarSidePanel extends Component {
  static propTypes = {
    /**
     * When true the side panel should be expanded
     */
    open: PropTypes.bool.isRequired,
    /** 
     * Click Handler for expander icon being clicked 
     */
    onExpanderClick: PropTypes.func
  }

  handleExpanderClick = evt => {
    this.props.onExpanderClick && this.props.onExpanderClick(evt) 
  }

  render() {
    const {
      open,
      children,
      onExpanderClick,
      ...rest
    } = this.props;
    return (
      <SidePanel open={open} {...rest}>
        <Expander
          open={open}
          onClick={this.handleExpanderClick}
        />
        { children }
        <MiniCalender 
          open={open}
          year={2018} 
          month={11}
          day={13}  
        />
      </SidePanel>
    )
  }
}
