import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { 
  addDays,
  addWeeks,
  getEndOfMonth,
  getTotalWeeks
} from '../util/date-helper'

const StyledCol = styled.div`
  flex: 1 1 ${ 100 / 7 }%;
  text-align: center;
  font-size: .75em;
  color: ${ props => props.isWeekend ? 'black' : 'inherit' };
`

const StyledDayCol = styled(StyledCol)`
  padding-top: ${ 100 / 7 }%;
  cursor: pointer;
  position: relative;
`

const StyledDayColHeader = styled(StyledCol)`

`

const StyledDay = styled.div`
  position: absolute;
  top: 0; right: 0; bottom: 0; left: 0;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: background-color .25s ease-out;
  background-color: ${ props => props.isToday ? 'rgba(0, 0, 0, .25)' : 'transparent' };
  :hover  {
    background-color: rgba(255, 255, 255, .5);
  }
`

const StyledMonth = styled.div`
  display: flex;
  flex-direction: column;
`

const StyledRow = styled.div`
  display: flex;
  align-items: stretch;
`
export default class MonthCalendar extends Component {

  static propTypes = {
    /**
     * The full year to show (e.g. 2018)
     */
    year: PropTypes.number,
    /**
     * The zero based month index to show (i.e. January = 0)
     */
    month: PropTypes.number
  }

  shouldComponentUpdate = (nextProps)  => {
    if(nextProps.year === this.props.year && 
      nextProps.month === this.props.month) {
        return false;
      }
    return true;
  }

  getPrevDayOfWeek = (date, dayOfWeek) => {
    let nextDate = new Date(date.getTime());

    let currentDay = nextDate.getDay();
    while(currentDay !== dayOfWeek) {
      nextDate = addDays(nextDate, -1);
      currentDay = nextDate.getDay();
    }

    return nextDate;
  }

  getNextDayOfWeek = (date, dayOfWeek) => {
    let nextDate = new Date(date.getTime());

    let currentDay = nextDate.getDay();
    while(currentDay !== dayOfWeek) {
      nextDate = addDays(nextDate, 1);
      currentDay = nextDate.getDay();
    }

    return nextDate;
  }

  renderDays(dateFrom, dateTo) {
    const now = new Date();
    const nowYear = now.getFullYear();
    const nowMonth = now.getMonth();
    const nowDay = now.getDate();
    let days = [];
    for(let date = dateFrom; date < dateTo; date = addDays(date, 1)) {
      const dayOfWeek = date.getDay();
      const isWeekend = dayOfWeek === 0 || dayOfWeek === 6;
      const isToday = nowYear === date.getFullYear() && nowMonth === date.getMonth() && nowDay === date.getDate();
      days.push(
        <StyledDayCol 
          key={date.getTime()}          
          isWeekend={isWeekend}
          isWeekday={!isWeekend}
          >
          <StyledDay isToday={isToday} >{ date.getDate() }</StyledDay>
        </StyledDayCol>
      )
    }
    return days;
  }

  renderWeeks() {
    const month = new Date(this.props.year, this.props.month);
    // get the first Monday
    const start = this.getPrevDayOfWeek(month, 1);
    
    const endOfMonth = getEndOfMonth(month);
    // get last Sunday
    const end = this.getNextDayOfWeek(endOfMonth, 0);

    const totalWeeks = Math.ceil(getTotalWeeks(start, end));

    let weeks = [];

    for(let week = 0; week < totalWeeks; week++) {
      weeks.push(
        <StyledRow key={week}>
          { this.renderDays(addWeeks(start, week), addWeeks(start, week + 1)) }
        </StyledRow>
      )
    }
    return weeks;
  }

  render() {
    const {
      year,
      month,
      children,
      ...rest
    } = this.props;

    const currentMonth = new Date(year, month);
    // get the first Monday
    const start = this.getPrevDayOfWeek(currentMonth, 1);

    const weekdayOptions = { weekday: 'narrow'};

    return (
      <StyledMonth {...rest}>
        <StyledRow>
          <StyledDayColHeader>{ addDays(start, 0).toLocaleDateString('en-gb', weekdayOptions) }</StyledDayColHeader>
          <StyledDayColHeader>{ addDays(start, 1).toLocaleDateString('en-gb', weekdayOptions) }</StyledDayColHeader>
          <StyledDayColHeader>{ addDays(start, 2).toLocaleDateString('en-gb', weekdayOptions) }</StyledDayColHeader>
          <StyledDayColHeader>{ addDays(start, 3).toLocaleDateString('en-gb', weekdayOptions) }</StyledDayColHeader>
          <StyledDayColHeader>{ addDays(start, 4).toLocaleDateString('en-gb', weekdayOptions) }</StyledDayColHeader>
          <StyledDayColHeader isWeekend={true}>{ addDays(start, 5).toLocaleDateString('en-gb', weekdayOptions) }</StyledDayColHeader>
          <StyledDayColHeader isWeekend={true}>{ addDays(start, 6).toLocaleDateString('en-gb', weekdayOptions) }</StyledDayColHeader>
        </StyledRow>
        { this.renderWeeks() }
      </StyledMonth>
    )
  }
}
