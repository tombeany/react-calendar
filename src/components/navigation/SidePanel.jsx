import React, { Component } from 'react'
import styled from 'styled-components'

const StyledPanel = styled.div.attrs(props => ({
  style: { width: props.open ? '220px' : '50px' }
}))`
  height: 100%;
  color: white;
  background-color: #263770;
  transition: width .25s ease-out;
  position: relative;
`

export default class SidePanel extends Component {
  render() {
    // const {
    //   open
    // } = this.props;

    return (
      <StyledPanel {...this.props} />
    )
  }
}
