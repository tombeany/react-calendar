import React from 'react'
import styled from 'styled-components'

const StyledPanel = styled.div`
  flex: 1 1 auto;
`

function AppPanel(props) {
  return (
    <StyledPanel {...props}/>
  )
};

export default AppPanel


