import React from 'react'
import styled from 'styled-components'

const StyledTimeIndicator = styled.div.attrs(props => ({
  style: { top: props.top }
}))`
  position: absolute;
  left: 0;
  width: 100%;
  height: 1px;
  background-color: red;
`

const StyledArrowLeft = styled.div`
  position: absolute;
  top: -5px;
  left: 0px;
  border: 5px solid transparent;
  border-left-color: red;
`

const StyledArrowRight = styled.div`
  position: absolute;
  top: -5px;
  right: 0px;
  border: 5px solid transparent;
  border-right-color: red;
`
class TimeIndicator extends React.Component {

  constructor(props) {
    super(props);

    this.tickHandle = undefined;
    this.handleTick = this.handleTick.bind(this);

    this.state = {
      pos: 0,
      expired: false
    };
  }

  componentDidMount() {
    this.handleTick();
    this.tickHandle = setInterval(this.handleTick, 1000);
  }

  componentWillUnmount() {
    this.clear();
  }

  handleTick = () => {
    const {
      from,
      to
    } = this.props;

    const now = new Date();
    if(now > to) {
      this.clear();
      this.setState({ expired: true });
      return;
    }

    const total = to - from;
    const timePos = Number((((now - from) / total) * 100).toFixed(1));
    if(timePos !== this.state.pos)
      this.setState({ pos: timePos });
  }

  clear = () => {
    clearInterval(this.tickHandle);
  }

  render() {
    if(this.state.expired === true)
      return null;
    return (
      <StyledTimeIndicator top={`${this.state.pos}%`}>
        <StyledArrowLeft />
        <StyledArrowRight />
      </StyledTimeIndicator>
    )
  }
}

export default TimeIndicator
