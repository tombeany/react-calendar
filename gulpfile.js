var gulp = require('gulp');
var svgSprite = require('gulp-svg-sprite');


function svgTask(cb) {

  var config = {
    mode: {
      symbol: { // Activate the «symbol» mode
      }
    },
    shape: {
      dimension: {
        maxWidth: 32,
        maxHeight: 32,
        precision: 2
      },
      spacing: {
        padding: 0
      },
      transform: [{
        svgo: {
          plugins: [
            { removeDoctype: true },
            { convertTransform: false },
            { removeUselessStrokeAndFill: false },
            { removeDimensions: false }
            
            // { inlineStyles: true },
            // { removeEmptyContainers: true },
            // { removeEmptyText: true },
            // { removeHiddenElems: true },
            // { removeEmptyAttrs: true },
            // { removeEditorsNSData: true },
            // { removeUselessDefs: true },
            // { removeDesc: true },
            // { removeTitle: true },
            // { removeMetadata: true },
            // { removeComments: true }
          ]
        }
      }]
    },
    svg: {
      transform: [{
        svgo: {
          plugins: [
            { removeDoctype: true },
            { convertTransform: false },
            { removeUselessStrokeAndFill: false },
            { removeDimensions: false }
            
            // { inlineStyles: true },
            // { removeEmptyContainers: true },
            // { removeEmptyText: true },
            // { removeHiddenElems: true },
            // { removeEmptyAttrs: true },
            // { removeEditorsNSData: true },
            // { removeUselessDefs: true },
            // { removeDesc: true },
            // { removeTitle: true },
            // { removeMetadata: true },
            // { removeComments: true }
          ]
        }
      }]
    }
  };

  gulp.src('**/*.svg', { cwd: 'src/icons'})
    .pipe(svgSprite(config))
    .pipe(gulp.dest('public/icons'))
    .on('error', function(error) {
      console.log(error);
    });
  
  cb();
}

exports.default = svgTask